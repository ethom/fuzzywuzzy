#!/usr/bin/env python
# encoding: utf-8
from . import fuzz
#from fuzzywuzzy import fuzz
#from fuzzywuzzy import utils
from . import utils
import heapq
import logging
from functools import partial
import itertools
from tqdm import tqdm


default_scorer = fuzz.WRatio


default_processor = utils.full_process

def no_process(x):
    return x

class extractor():
    #based on extractorwithoutorder in fuzzywuzzy.process
    def __init__(self,init_choices, processor=default_processor, scorer=default_scorer, score_cutoff=0,match_memory=True,dynamic_memory=True):
        self.score_cutoff=score_cutoff
        self.processor = processor #might be turned into identity.


        # Don't run full_process twice
        if scorer in [fuzz.WRatio, 
                        fuzz.QRatio,
                        #
                        fuzz.token_set_ratio,
                        fuzz.token_sort_ratio,
                        fuzz.partial_token_set_ratio,
                        fuzz.partial_token_sort_ratio]:
            
            ##DEV verify that full_process IS and IF IT NEEDS to be run.
            self.pre_processor = partial(utils.full_process, force_ascii=True)
            self.scorer = partial(scorer, full_process=False)

            if processor == utils.full_process:
                self.processor = lambda x: x
                #self.processor = no_process

        elif scorer in [fuzz.UWRatio,
                         fuzz.UQRatio]:

            ##Isn't this identical to above?
            self.pre_processor = partial(utils.full_process, force_ascii=False)
            self.scorer = partial(scorer, full_process=False)

            #No predetermine pre_processor/scorer 
            if processor == utils.full_process:
                self.processor = lambda x: x
                #self.processor = no_process

        else:
            self.pre_processor = no_process
            ## ^^^ Notice it's PRE_processor..
       
        ##All processing is done PRIOR to matching,
        # and saved alongside unprocessed part.
        choices_dict = dict()
        for key, choice in init_choices.items():
            processed_choice = self.processor(choice)
            processed_choice = self.pre_processor(processed_choice) #DEV HERE
            #vvv slopy.. include key in key's values?
            choices_dict[key] = (choice, key, processed_choice) #DEV HERE
        
        self.choices = choices_dict

        if match_memory == True:
            self.match = self.match_with_memory
            self.score_dict = dict()
            if not dynamic_memory == True:
                for combo in itertools.combinations(self.choices,2):
                    key = "".join(sorted(combo))
                    self.score_dict[key] = "100"
                    # print(len(list(self.score_dict)))

        else:
            self.match = self.match_without_memory

    def match_without_memory(self,query):
        import time
        query_key = query[1]
        processed_query = query[2]

        for choice_key, choice_tuple in self.choices.items():
            score = self.scorer(processed_query, choice_tuple[2])  #DEV HERE

            if score >= self.score_cutoff:
                yield (choice_tuple[0], score, choice_key)


    def match_with_memory(self,query):
        ## query is pop'd out of extractor.choices
        #   - choices[key_i] == (choice, processed_choice)
        #   => don't have to pre_process query.
        #processed_query = self.processor(query) #line 78
        #processed_query = self.pre_processor(processed_query) #line 104
        query_key= query[1]
        processed_query = query[2]

        for choice_key, choice_tuple in self.choices.items():
            pair_key = [choice_key, query_key]
            pair_key.sort()
            pair_key = "".join(pair_key)
            #unprocessed_choice, processed_choice = choice_tuple
            #^^ does this slow things way down??
            #(choice, processed_choice)

            try:
                score = self.score_dict[pair_key]

                try:
                    if score >= self.score_cutoff:
                    #raise NameError
                        yield (choice_tuple[0], score, choice_key)

                except TypeError: #For Pre-allocated Memory ... awk because requires memory to be of a different type.
                    raise KeyError

            except KeyError: #For Dynamically allocated Memory
               score = self.scorer(processed_query, choice_tuple[2])  #DEV HERE
               self.score_dict[pair_key] = score
               if score >= self.score_cutoff:
                   yield (choice_tuple[0], score, choice_key)

